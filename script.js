function toggleMenu() {
    const menu = document.querySelector('.nav-menu');
    menu.style.display = menu.style.display === 'block' ? 'none' : 'block';
  }
  
  document.addEventListener('click', function(event) {
    const menu = document.querySelector('.nav-menu');
    const menuIcon = document.querySelector('.menu-icon');
    if (!menu.contains(event.target) && !menuIcon.contains(event.target)) {
      menu.style.display = 'none';
    }
  });
  
  function performSearch(event) {
    event.preventDefault();
    const query = document.getElementById('search-query').value.toLowerCase();
    const resultsContainer = document.getElementById('search-results');
  
    // Simulated search results
    const posts = [
      { title: 'Post 1', content: 'This is the content of post 1.' },
      { title: 'Post 2', content: 'This is the content of post 2.' },
      { title: 'Post 3', content: 'This is the content of post 3.' },
    ];
  
    const results = posts.filter(post => post.title.toLowerCase().includes(query) || post.content.toLowerCase().includes(query));
  
    resultsContainer.innerHTML = '';
  
    if (results.length > 0) {
      results.forEach(result => {
        const resultItem = document.createElement('div');
        resultItem.className = 'result-item';
        resultItem.innerHTML = `
          <h3>${result.title}</h3>
          <p>${result.content}</p>
        `;
        resultsContainer.appendChild(resultItem);
      });
    } else {
      resultsContainer.innerHTML = '<h2>No results found</h2>';
    }
  }

  // JavaScript code goes here
  function showAlert() {
    alert("This section is under development!");
  }
  